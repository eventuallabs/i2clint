* Inter-chain conflict
  :PROPERTIES:
  :COLUMNS: %25ITEM %i2c_chain(chain) %device %pcb_name(PCB)
  :END:
  - This file can be scanned with i2clint


** I2C switch
   :PROPERTIES:
   :device:   PCA9543A
   :manufacturer: Texas Instruments
   :pcb_pn:   90069
   :pcb_name: Topaz A
   :pcb_revision: a
   :i2c_chain: M 0x70.0 0x70
   :is_mux:   true
   :END:
   - This is the i2c mux on the Topaz board.  All devices on Topaz A
     come off channel 0 of the Whitfield I2C mux.
   - There are two address pins, which can each be high or low.
|----+----+---------|
| A1 | A0 | Address |
|----+----+---------|
|  0 |  0 |    0x70 |
|  0 |  1 |    0x71 |
|  1 |  1 |    0x73 |
|----+----+---------|
