#!/opt/ActiveTcl-8.6/bin/tclsh
# Hey Emacs, use -*- Tcl -*- mode

set scriptname [file rootname $argv0]

set thisfile [file normalize [info script]]

# Directory where this script lives
set program_directory [file dirname $thisfile]

# Directory from which the script was invoked
set invoked_directory [pwd]

source ${program_directory}/lib/org.tcl

# ---------------------- Command line parsing -------------------------
package require cmdline
set usage "usage: [file tail $argv0] \[options] filename"
set options {
    {i.arg "all" "Quoted list of boards (PCB names) to include"}
}

try {
    array set params [::cmdline::getoptions argv $options $usage]
} trap {CMDLINE USAGE} {msg o} {
    # Trap the usage signal, print the message, and exit the application.
    # Note: Other errors are not caught and passed through to higher levels!
    puts $msg
    exit 1
}

set board_list $params(i)

# After cmdline is done, argv will point to the last argument
if {[llength $argv] == 1} {
    set orgfile $argv
} else {
    puts [cmdline::usage $options $usage]
    exit 1
}

proc iterint {start points} {
    # Return a list of increasing integers starting with start with
    # length points
    set count 0
    set intlist [list]
    while {$count < $points} {
	lappend intlist [expr $start + $count]
	incr count
    }
    return $intlist
}

proc dashline {width} {
    # Return a string of dashes of length width
    set dashline ""
    foreach dashchar [iterint 0 $width] {
	append dashline "-"
    }
    return $dashline
}

proc get_org_entry_list {orgfile} {
    # Return the list of I2C devices
    #
    # This is the raw list output from parsing the org file.  We'll
    # need to index into these list elements to populate a device
    # dictionary.
    #
    # Arguments:
    #   orgfile -- Input org-mode file
    set rawlist [org::parseOrgFile $orgfile]
    # |-------+--------------------------------|
    # | Index | Use                            |
    # |-------+--------------------------------|
    # |     0 | Headline level                 |
    # |     1 | Headline                       |
    # |     2 | List of tags                   |
    # |     3 | List of property:value         |
    # |     4 | List of lines in the body text |
    # |     5 | List of child nodes            |
    # |-------+--------------------------------|
    foreach entry $rawlist {
	# rawlist is a list of level-1 headers.  Each list index
	# contains a list of level-1 attributes (described above),
	# which includes a list of level-2 headers.
	#
	# We'll have a single level-one header (*).  Each device will
	# be a level-two child (**).  So index 0 in the top-level list
	# will be all we care about.  But we still have to loop
	# through all the list elements to filter out garbage
	# top-level lists returned by parseOrgFile.
	set childlist [lindex $entry 0]
	if {[llength $childlist] == 0} {
	    # This is garbage.
	    continue
	}
	# The fifth element in the master list will be the list of child
	# nodes.  This is our list of devices.
	set org_entry_list [lindex $childlist 5]
    }
    return $org_entry_list
}

proc get_i2c_dict {org_entry_list} {
    # Return a dictionary of i2c_address : attributes
    foreach device $org_entry_list {
	# Each device will have
	# |-------+--------------------------------|
	# | Index | Use                            |
	# |-------+--------------------------------|
	# |     0 | Headline level                 |
	# |     1 | Headline                       |
	# |     2 | List of tags                   |
	# |     3 | List of property:value         |
	# |     4 | List of lines in the body text |
	# |     5 | List of child nodes            |
	# |-------+--------------------------------|
	set property_list [lindex $device 3]
	set description [lindex $device 1]
	# Find the i2c address first, so it can be the key
	foreach {key value} $property_list {
	    if {[string equal "i2c_chain" $key]} {
		# We'll keep track of addresses in hex format.  The last
		# address in the chain is the device's address.
		set address_string [lindex $value end]
		set address 0x[format "%x" $address_string]
		dict set i2c_dict $address i2c_chain $value
	    }
	}
	# Now populate the rest of the dictionary
	foreach {key value} $property_list {
	    if {[string equal "pcb_pn" $key]} {
		# PCB part number
		set pn $value
		dict set i2c_dict $address pcb_pn $pn
	    }
	    if {[string equal "pcb_name" $key]} {
		# PCB name
		set pcb_name $value
		dict set i2c_dict $address pcb_name $pcb_name
	    }
	    if {[string equal "pcb_revision" $key]} {
		# PCB revision -- set this to upper case
		set pcb_revision [string toupper $value]
		dict set i2c_dict $address pcb_revision $pcb_revision
	    }
	    if {[string equal "device" $key]} {
		# Name of the IC or I2C slave
		set device $value
		dict set i2c_dict $address device $device
	    }
	}
    }
    return $i2c_dict
}

proc get_device_dict {org_entry_list} {
    # Return a dictionary of device_id : attributes
    #
    # Each device ID indexes a list of key : value pairs.  To get a
    # value, use [dict get $device_dict $device_id $key]
    #
    # Arguments:
    #   org_entry_list -- List of devices as output by get_org_entry_list
    set device_id 0
    foreach device $org_entry_list {
	# Each device will have
	# |-------+--------------------------------|
	# | Index | Use                            |
	# |-------+--------------------------------|
	# |     0 | Headline level                 |
	# |     1 | Headline                       |
	# |     2 | List of tags                   |
	# |     3 | List of property:value         |
	# |     4 | List of lines in the body text |
	# |     5 | List of child nodes            |
	# |-------+--------------------------------|
	set property_list [lindex $device 3]
	set headline [lindex $device 1]
	dict set device_dict $device_id description $headline
	foreach {key value} $property_list {
	    if {[string equal "i2c_chain" $key]} {
		# We'll keep track of addresses in hex format.  The last
		# address in the chain is the device's address.
		dict set device_dict $device_id i2c_chain $value
	    }
	    if {[string equal "pcb_pn" $key]} {
		# PCB part number
		set pn $value
		dict set device_dict $device_id pcb_pn $pn
	    }
	    if {[string equal "pcb_name" $key]} {
		# PCB name
		set pcb_name $value
		dict set device_dict $device_id pcb_name $pcb_name
	    }
	    if {[string equal "pcb_revision" $key]} {
		# PCB revision -- set this to upper case
		set pcb_revision [string toupper $value]
		dict set device_dict $device_id pcb_revision $pcb_revision
	    }
	    if {[string equal "device" $key]} {
		# Name of the IC or I2C slave
		set device $value
		dict set device_dict $device_id device $device
	    }
	    if {[string equal "is_mux" $key]} {
		# Is this a multiplexer?
		set is_mux $value
		dict set device_dict $device_id is_mux $is_mux
	    }
	}
	incr device_id
    }
    return $device_dict
}

proc check_device_dict {device_dict} {
    # Check the device dictionary for problems
    foreach id [dict keys $device_dict] {
	try {
	    set pcb_name [dict get $device_dict $id pcb_name]
	} trap {} {message optdict} {
	    puts "ERROR: Part $id has no pcb_name attribute"
	    exit
	}
	try {
	    set i2c_chain [dict get $device_dict $id i2c_chain]
	} trap {} {message optdict} {
	    puts "ERROR: Part $id has no i2c_chain attribute"
	    exit
	}
	try {
	    set description [dict get $device_dict $id description]
	} trap {} {message optdict} {
	    puts "ERROR: Part $id has no description attribute"
	    exit
	}
	if {[lindex $i2c_chain 0] != "M"} {
	    # First item in i2c_chain is not M for Master
	    puts "ERROR: First link in i2c_chain is not M for $description on $pcb_name"
	    exit
	}

    }
    return

}

proc pretty_puts_dict {dict {pattern *}} {
    set longest 0
    dict for {key -} $dict {
	if {[string match $pattern $key]} {
	    set longest [expr {max($longest, [string length $key])}]
	}
    }
    dict for {key value} [dict filter $dict key $pattern] {
	puts [format "%-${longest}s = %s" $key $value]
    }
}

proc get_intrachain_conflicts {device_dict board_list} {
    # Return a list of error strings for intrachain conflicts
    #
    # There can be no repeated addresses inside a chain
    # set address_dict [dict create]
    #
    # Arguments:
    #   device_dict -- Dictionary of device_id : attributes as returned by
    #                  get_device_dict
    #   board_list -- List of boards from command line, default is all
    puts "* Looking for conflicts inside chains"
    set conflict_list [list]
    foreach id [dict keys $device_dict] {
	set pcb_name [dict get $device_dict $id pcb_name]
	# Look for the pcb_name in the list of included boards
	if {[lsearch $board_list $pcb_name] < 0 && \
		![string match "all" $board_list] && \
		![string match "none" $pcb_name]} {
	    # The device is on a board that's not on the list of included
	    # boards.  Go on to the next one.
	    continue
	}
	set description [dict get $device_dict $id description]
	try {
	    set i2c_chain [dict get $device_dict $id i2c_chain]
	} trap {} {message optdict} {
	    puts "ERROR: $description on $pcb_name has no i2c_chain attribute"
	    exit
	}
	# Add up the ocurrances of each address
	set counters [dict create]
	foreach address $i2c_chain {
	    dict incr counters [lindex [split $address "."] 0]
	}
	foreach address [dict keys $counters] {
	    set occurrances [dict get $counters $address]
	    if {$occurrances > 1} {
		lappend conflict_list \
		    "Device $id with address $address has a duplicate in its chain"
	    }
	}
    }
    return $conflict_list
}

proc get_master_level_conflicts {device_dict board_list} {
    # Return a list of error strings for devices seen directly by the master
    #
    # The master can not see two devices with the same i2c address
    # adjacent to it.  This means that there can be no identical
    # chains with a chain length of 2.
    #
    # Arguments:
    #
    #   device_dict -- Dictionary of device_id : attributes as returned by
    #                  get_device_dict
    #   board_list -- List of boards from command line, default is all
    puts "* Looking for address conflicts next to the master"
    set conflict_list [list]
    set counter_dict [dict create]
    foreach id [dict keys $device_dict] {
	set pcb_name [dict get $device_dict $id pcb_name]
	# Look for the pcb_name in the list of included boards
	if {[lsearch $board_list $pcb_name] < 0 && \
		![string match "all" $board_list] && \
		![string match "none" $pcb_name]} {
	    # The device is on a board that's not on the list of included
	    # boards.  Go on to the next one.
	    continue
	}
	set i2c_chain [dict get $device_dict $id i2c_chain]
	set description [dict get $device_dict $id description]
	if {[llength $i2c_chain] > 2} {
	    # We're only looking for chains of length 2.  Skip chains longer than this.
	    continue
	}
	set i2c_address [lindex $i2c_chain end]
	if {[lsearch [dict keys $counter_dict] $i2c_address] >= 0} {
	    # This device's i2c address has already been found
	    lappend conflict_list \
		"Two devices with address $i2c_address are connected to the master"
	} else {
	    # We need to add this device to the dictionary
	    dict set counter_dict $i2c_address count 0
	}
    }
    return $conflict_list
}

proc get_interchain_conflicts {device_dict board_list} {
    # Return a list of error strings for interchain conflicts
    #
    # Arguments:
    #
    #   device_dict -- Dictionary of device_id : attributes as returned by
    #                  get_device_dict
    #   board_list -- List of boards from command line, default is all
    puts "* Looking for address conflicts between chains"
    set conflict_list [list]
    set counter_dict [dict create]
    foreach id [dict keys $device_dict] {
	set pcb_name [dict get $device_dict $id pcb_name]
	# Look for the pcb_name in the list of included boards
	if {[lsearch $board_list $pcb_name] < 0 && \
		![string match "all" $board_list] && \
		![string match "none" $pcb_name]} {
	    # The device is on a board that's not on the list of included
	    # boards.  Go on to the next one.
	    continue
	}
	set i2c_chain [dict get $device_dict $id i2c_chain]
	set head_i2c_address [lindex $i2c_chain 1]
	set is_mux [dict get $device_dict $id is_mux]
	set description [dict get $device_dict $id description]
	if {[lsearch -glob -nocase [dict keys $counter_dict] $head_i2c_address] >= 0} {
	    # The head of this device's chain has already been found
	    if ![dict get $counter_dict $head_i2c_address is_mux] {
		# The previous address found was not a mux, so this is a conflict.
		lappend conflict_list \
		    "Address $head_i2c_address has a conflict outside its chain"
	    }
	} else {
	    # We need to add this device to the dictionary
	    dict set counter_dict $head_i2c_address count 0
	    dict set counter_dict $head_i2c_address is_mux $is_mux
	}
    }
    return $conflict_list
}

proc get_simultaneous_chain_list {device_dict} {
    # Return a list of simultaneous chain lists
    #
    # Chains within simultaneous chain lists exist simultaneously.
    # Chains are identified by their device IDs -- top-level keys in
    # the device_dict.
    #
    # Arguments:
    #   device_dict -- Dictionary of device_id : attributes as returned by
    #                  get_device_dict
    #
    set simultaneous_list [list]
    foreach out_id [dict keys $device_dict] {
	set used_ids [list]

	set this_simultaneous_list [list $out_id]
	lappend used_ids $out_id
	set i2c_chain [dict get $device_dict $out_id i2c_chain]
	if {[llength $i2c_chain] == 2} {
	    # This chain just contains the master and the endpoint --
	    # no switches.  We'll catch these problems another way.
	    continue
	}
	set out_mux_list [lrange $i2c_chain 1 end-1]
	foreach in_id [dict keys $device_dict] {
	    if {$in_id == $out_id} {
		continue
	    }
	    set i2c_chain [dict get $device_dict $in_id i2c_chain]
	    if {[llength $i2c_chain] == 2} {
		# This chain just contains the master and the endpoint --
		# no switches.  We'll catch these problems another way.
		continue
	    }
	    set in_mux_list [lrange $i2c_chain 1 end-1]
	    if {[join $out_mux_list] eq [join $in_mux_list]} {
		# All mux switches are the same, so these are simultaneous lists
		lappend this_simultaneous_list $in_id
	    }
	}
	if {[llength $this_simultaneous_list] > 1} {
	    # Single-element lists are always simultaneous with
	    # themselves, so we'll skip them.
	    set is_duplicate false
	    # Look for permutations of this device list in the
	    # existing simultaneous device list.  Don't add
	    # permutations.
	    foreach device_list $simultaneous_list {
		if {[is_permuted_list $this_simultaneous_list $device_list]} {
		    set is_duplicate true
		}
	    }
	    if {! $is_duplicate} {
		lappend simultaneous_list $this_simultaneous_list
	    }

	}
    }
    return $simultaneous_list
}

proc is_permuted_list {list1 list2} {
    if {[llength $list1] == [llength $list2]} {
	foreach number $list1 {
	    if {$number in $list2} {
		continue
	    } else {
		return false
	    }
	}
	return true
    } else {
	return false
    }
}

proc get_simultaneous_chain_conflicts {device_dict simultaneous_list} {
    # Return a list of error strings for devices at the ends of simultaneous chains
    #
    # Arguments:
    #
    #   device_dict -- Dictionary of device_id : attributes as returned by
    #                  get_device_dict
    #   simultaneous_list -- List of simultaneous chain sets as returned by
    #                        get_simultaneous_chain_list
    set conflict_list [list]
    foreach simultaneous_set $simultaneous_list {
	puts "* Looking for conflicts in simultaneous chains [join $simultaneous_set " "]"
	set counter_dict [dict create]
	foreach device_id $simultaneous_set {
	    set i2c_address [lindex [dict get $device_dict $device_id i2c_chain] end]
	    if {[lsearch [dict keys $counter_dict] $i2c_address] >= 0} {
		# This device's i2c address has already been found
		lappend conflict_list \
		    "Device $device_id with address $i2c_address is causing a simultaneous chain conflict"
	    } else {
		# We need to add this device to the dictionary
		dict set counter_dict $i2c_address count 0
	    }
	}

    }
    return $conflict_list
}

# Get the list of I2C devices
set org_entry_list [get_org_entry_list $orgfile]

# Get a dictionary of devices
#
# You might be tempted to make a dictionary of I2C address :
# attributes, but this won't work.  There can be many address
# duplicates in a working system.
set device_dict [get_device_dict $org_entry_list]

check_device_dict $device_dict

set simultaneous_chain_list [get_simultaneous_chain_list $device_dict]

# Report addresses

# The keys in table_dict must match those in device dict.  Order these
# items in the order they'll appear in the table.

dict set table_dict device_id width 5
dict set table_dict device_id header "ID"

dict set table_dict description width 30
dict set table_dict description header "Description"

dict set table_dict device width 20
dict set table_dict device header "Device"

dict set table_dict pcb_pn width 10
dict set table_dict pcb_pn header "PCB PN"

dict set table_dict pcb_name width 10
dict set table_dict pcb_name header "PCB name"

dict set table_dict pcb_revision header "Rev"
dict set table_dict pcb_revision width 5

dict set table_dict i2c_chain width 10
dict set table_dict i2c_chain header "Chain"

foreach column [dict keys $table_dict] {
    append format_string "%-*s "
    lappend header_list [dict get $table_dict $column width]
    lappend header_list [dict get $table_dict $column header]
}
set format_string [string trim $format_string]

set header [format $format_string {*}$header_list]

puts ""
puts $header

puts [dashline [string length $header]]
foreach id [dict keys $device_dict] {
    set pcb_name [dict get $device_dict $id pcb_name]
    # Look for the pcb_name in the list of included boards
    if {[lsearch $board_list $pcb_name] < 0 && \
	    ![string match "all" $board_list] && \
	    ![string match "none" $pcb_name]} {
	# The device is on a board that's not on the list of included
	# boards.  Go on to the next one.
	continue
    }
    set row_list [list]
    foreach column [dict keys $table_dict] {
	lappend row_list [dict get $table_dict $column width]
	if {$column eq "device_id"} {
	    lappend row_list $id
	    continue
	}
	try {
	    lappend row_list [dict get $device_dict $id $column]
	} trap {TCL LOOKUP DICT} {message optdict} {
	    # The device doesn't have this attribute
	    lappend row_list ""
	}
    }
    puts [format $format_string {*}$row_list]
}

# Report conflicts

# Look for identical i2c addresses adjacent to the master
puts ""
lappend conflict_list [get_master_level_conflicts $device_dict $board_list]

lappend conflict_list [get_intrachain_conflicts $device_dict $board_list]

lappend conflict_list [get_simultaneous_chain_conflicts $device_dict $simultaneous_chain_list]

set flat_conflict_list [list]
foreach conflict_class $conflict_list {
    foreach conflict $conflict_class {
	lappend flat_conflict_list $conflict
    }
}

puts ""
if {[llength $flat_conflict_list] != 0} {
    foreach conflict $flat_conflict_list {
	puts "error --> $conflict"
    }
} else {
    puts "No address conflicts"
}

