namespace eval org {


    proc parseLineIntoHeadlineAndTags {line preamble} {
	# Given a line containing a preamble, some text, and possibly some tags,
	# return the headline alone and the tags list
	set tagsList {}
	set headline {}

	set preambleLen [string length $preamble]

	if {$preambleLen == 0} {
	    set headlineAndTags $line
	} else {
	    set headlineAndTags [string range $line $preambleLen end]
	}

	# Extract the tags
	# Tags are normal words containing letters, numbers, `_', and `@'
	# So the regexp to find them would be:
	# :[:a-zA-Z0-9_@]*:
	
	set allTags {}
	regexp {:[:a-zA-Z0-9_@]*:} $headlineAndTags allTags

	# TEMPORARY
	# Also pull out / and - and normalize them.
	#regexp {:[:a-zA-Z0-9_@\-\/]*:} $headlineAndTags allTags
	#set allTags [regsub -all {[\-\/]} $allTags "_" ]
	
	if {[string length $allTags] > 0} {
	    
	    set break [string first $allTags $headlineAndTags]

	    set headline [string trim [string range $headlineAndTags 0 [expr {$break-1}]]]
	    #set headline [string range $headlineAndTags 0 [expr {$break-1}]]

	    set tagsList [split [string trim $allTags :] :]
	} else {
	    set allTags {}

	    set headline [string trim $headlineAndTags]
	    #set headline $headlineAndTags

	    set tagsList {}
	}

	return [list $headline $tagsList]
    }

    proc extractPropertiesFromBody {body} {
	# Grab the properties block from the body
	# return [newbody properties]
	#
	# Properties should not be one of (as these are reserved by Org):
	#      TODO         The TODO keyword of the entry.
	#      TAGS         The tags defined directly in the headline.
	#      ALLTAGS      All tags, including inherited ones.
	#      CATEGORY     The category of an entry.
	#      PRIORITY     The priority of the entry, a string with a single letter.
	#      DEADLINE     The deadline time string, without the angular brackets.
	#      SCHEDULED    The scheduling timestamp, without the angular brackets.
	#      CLOSED       When was this entry closed?
	#      TIMESTAMP    The first keyword-less timestamp in the entry.
	#      TIMESTAMP_IA The first inactive timestamp in the entry.
	#      CLOCKSUM     The sum of CLOCK intervals in the subtree.  org-clock-sum
	#                   must be run first to compute the values.
	#      BLOCKED      "t" if task is currently blocked by children or siblings
	#      ITEM         The content of the entry.
	set properties {}
	set newBody $body

	set foundBegin -1
	set foundEnd -1

	# Try to find the beginning and ending of the properties
	# block
	set lineNum 0
	foreach line $body {
	    if {($foundBegin == -1)&&([string trim $line] == ":PROPERTIES:")} {
		set foundBegin $lineNum
	    } elseif {($foundEnd == -1)&&([string trim $line] == ":END:")} {
		set foundEnd $lineNum
	    }
	    incr lineNum
	}

	if {($foundBegin != -1)&&($foundEnd != -1)&& \
		($foundEnd > $foundBegin)} {
	    set propertyLines [lrange $body $foundBegin $foundEnd]
	    set newBody [lreplace $body $foundBegin $foundEnd]

	    # Now figure out the properties in propertyLines
	    foreach line $propertyLines {
		set trimmedLine [string trim $line]
		if {($trimmedLine == ":PROPERTIES:") || \
			($trimmedLine == ":END") || \
			($trimmedLine == "")} {
		    continue
		}

		regexp {^\s*:([a-zA-Z0-9_@]*):\s*(.*)} $trimmedLine \
		    matched propName propValue
		set propValue [string trim $propValue]
		if {($propName != "")&&($propValue != "")} {
		    lappend properties $propName $propValue
		}
	    }
	}

	return [list $newBody $properties]
    }

    proc parseLinesIntoOrg {linebuf level} {
	# Parse a buffer of text lines into org items.
	# Each org item is a list composed of:
	#
	# {headline tags properties body children}


	#puts "DBG: parseLinesIntoOrg: level = $level"

	if {$level == 0} {
	    set preamble ""
	} else {
	    set preamble "[string repeat * $level] "
	}

	set nextlevel [expr {$level+1}]
	set nextpreamble "[string repeat * $nextlevel] "

	#puts "DBG: nextpreamble='$nextpreamble'"

	set breakingLines {}

	# Do we have a case of empty headline?
	set emptyHeadline 0
	if {($level == 0) && ([string index [lindex $linebuf 0] 0] == {*})} {
	    set emptyHeadline 1
	} elseif {($level > 0) && \
		      ([string first $preamble [lindex $linebuf 0]] != 0)} {
	    set emptyHeadline 1
	}

	#puts "DBG: emptyHeadline=$emptyHeadline"

	if {$emptyHeadline} {
	    set searchForNextLevelFrom 0
	} else {
	    set searchForNextLevelFrom 1
	}

	# Find the first instance of any next level below this one
	set nLines [llength $linebuf]
	for {set line $searchForNextLevelFrom} {$line < $nLines} {incr line} {
	    if {[regexp {^\*+ } [lindex $linebuf $line]]} {
		break
	    }
	}
	
	if {$line < $nLines} {

	    lappend breakingLines $line

	    # Find all the remaining instances of the next level
	    for {incr line} {$line < $nLines} {incr line} {
		if {[string first $nextpreamble [lindex $linebuf $line]] == 0} {
		    lappend breakingLines $line
		}
	    }
	}

	# Now we have:
	# linebuf[0] containing the headline and tags for this level
	# a list breakingLines {x y z ... } containing the line #s for the 
	#        child levels of this level
	# potentially lines 1-x containing additional text for this level

	if {$emptyHeadline == 0} {
	    foreach {headline tags} \
		[parseLineIntoHeadlineAndTags [lindex $linebuf 0] $preamble] {}
	} else {
	    set headline {}
	    set tags {}
	}

	set properties {}

	# Parse and store the body text.
	set body {}
	set firstLineOfBody 1
	if {[llength $breakingLines] > 0} {
	    set lastLineOfBody [expr {[lindex $breakingLines 0] - 1}]
	} else {
	    set lastLineOfBody [expr {[llength $linebuf] - 1}]
	}

	if {$lastLineOfBody >= $firstLineOfBody} {
	    for {set index $firstLineOfBody} \
		{$index <= $lastLineOfBody} \
		{incr index} {
		    set line [lindex $linebuf $index]
		    lappend body $line
		}
	}

	foreach {body properties} [extractPropertiesFromBody $body] {}

	set children {}
	set nChildren [llength $breakingLines]
	set lastIndex [expr {$nChildren-1}]
	for {set index 0} {$index < $nChildren} {incr index} {
	    set fromLine [lindex $breakingLines $index]
	    if {$index < $lastIndex} {
		set toLine [lindex $breakingLines [expr {$index+1}]]
		incr toLine -1
	    } else {
		set toLine end
	    }

	    set childlinebuf [lrange $linebuf $fromLine $toLine]
	    lappend children [parseLinesIntoOrg $childlinebuf $nextlevel]
	}

	return [list $level $headline $tags $properties $body $children]
    }



    proc parseTextIntoOrg {buf {level 0}} {
	# Parse a buffer of text into an org outline tree
	#
	# An org tree at level N consists of the following:
	# * _at the beginning of a line only_, N stars plus a space, e.g.
	#   "*** " at level 3
	#   (level 0 has no space)
	# * Headline
	# * Tags
	# * Properties
	# * Body
	# * Children
	# 
	# and will be returned as a TCL list in the following form:
	# {level headline tags properties body children}
	#
	# where:
	# level is a number
	# headline is the node's headline
	# tags is a list of tags appropriate to this node (and its children)
	# properties is a list of key/value pairs
	# body is a list of lines of the body text of the node
	# children is a list of the child nodes of this node
	#	
	set docHeader ""
	set docText ""
	set gotDocHeaderAndText 0
	
	set buf [split $buf "\n"]
	return [parseLinesIntoOrg $buf $level]
    }

    proc parseOrgFile {filename} {
	# Return org tree as a TCL list from the input file
	if {$filename == ""} {
	    set chan stdin
	} else {
	    set chan [open $filename]
	}
	set rv [parseTextIntoOrg [read $chan]]
	
	if {$filename != ""} {
	    close $chan
	}
	return $rv
    }
}
